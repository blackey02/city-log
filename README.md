# README #

### What is this repository for? ###

* The City Log app allows you to connect to online photo storage clouds, grab the metadata from each photo, and then produces a kml file that essentially lists the different cities you've been to. Currently Google Photos is the only cloud service supported although it's been designed to support other platforms with minimal work.
* 1.0.0
* [Video Demonstration](https://goo.gl/hEiTTk)
* Download the latest binary [here](https://bitbucket.org/blackey02/city-log/downloads)

### How do I get set up? ###

* Visual Studio 2015
* git clone https://bitbucket.org/blackey02/city-log.git
* Open CityLog.sln
* Create a new project in Google developer console and put in your client id, client secret, and redirect uri into CityLog\Services\Google\Auth\Auth.cs

### Who do I talk to? ###

* Blackey

### Props ###

* [GeoSharp](https://github.com/Necrolis/GeoSharp)
* [Google Picasa](https://developers.google.com/picasa-web/)
* [Google Plus](https://developers.google.com/+/web/api/rest/)