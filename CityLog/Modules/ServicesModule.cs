﻿using System;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using CityLog.Services.PhotoRepo;
using CityLog.Services.Google;
using CityLog.Services.GeoCoding;
using CityLog.Services.KmlGen;

namespace CityLog.Modules
{
    class ServicesModule : ModuleBase
    {
        public ServicesModule()
            : base()
        { }

        public override void Initialize()
        {
            Container.RegisterType<IPhotoRepo, GoogleRepo>(nameof(GoogleRepo), new ContainerControlledLifetimeManager());
            Container.RegisterType<IKmlGen, KmlGen>();
            Container.RegisterInstance(Container.Resolve<PhotoRepoFactory>());
            Container.RegisterInstance<IGeoCode>(Container.Resolve<GeoCode>());
        }
    }
}

