﻿using CityLog.Services.Google;
using CityLog.Services.Google.Auth;
using CityLog.Services.Google.Common;
using CityLog.Services.Google.Photos;
using CityLog.Services.Google.Plus;
using Microsoft.Practices.Unity;
using Prism.Modularity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Modules
{
    class GoogleModule : ModuleBase
    {
        public GoogleModule()
            : base()
        { }

        public override void Initialize()
        {
            Container.RegisterType<IGoogleRepo, GoogleRepo>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IAuth, Auth>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IPlus, Plus>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IPhotos, Photos>(new ContainerControlledLifetimeManager());
            Container.RegisterInstance<IService>(nameof(Plus), Container.Resolve<IPlus>());
            Container.RegisterInstance<IService>(nameof(Photos), Container.Resolve<IPhotos>());
            Container.RegisterType<Func<string, IService>>(
                new InjectionFactory(c => new Func<string, IService>(name => c.Resolve<IService>(name))));
        }
    }
}
