﻿using Microsoft.Practices.Unity;
using Prism.Modularity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Modules
{
    abstract class ModuleBase : IModule
    {
        [Dependency]
        public IUnityContainer Container { get; set; }

        public ModuleBase()
        { }

        public abstract void Initialize();
    }
}
