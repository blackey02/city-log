﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.DataTypes
{
    class HasStarted : BindableBase
    {
        private bool _geoCodes;

        public HasStarted()
        { }

        public bool GeoCodes
        {
            get { return _geoCodes; }
            set { SetProperty(ref _geoCodes, value); OnPropertyChanged(nameof(DidStart)); }
        }

        public bool DidStart
        {
            get
            {
                bool started = true;
                started = started && GeoCodes;
                return started;
            }
        }
    }
}
