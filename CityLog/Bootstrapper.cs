﻿using Microsoft.Practices.Unity;
using Prism.Unity;
using System.Windows;
using CityLog.Views;
using Prism.Modularity;
using CityLog.Modules;
using System.Globalization;
using CityLog.DataTypes;

namespace CityLog
{
    public class Bootstrapper : UnityBootstrapper
    {
        public Bootstrapper()
        {
            System.Threading.ThreadPool.SetMinThreads(10, 4);
            System.Net.ServicePointManager.DefaultConnectionLimit = 10;
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
        }

        protected override void ConfigureModuleCatalog()
        {
            base.ConfigureModuleCatalog();
            var googleType = typeof(GoogleModule);
            ModuleCatalog.AddModule(new ModuleInfo(googleType.Name, googleType.AssemblyQualifiedName));
            var servicesType = typeof(ServicesModule);
            ModuleCatalog.AddModule(new ModuleInfo(servicesType.Name, servicesType.AssemblyQualifiedName));
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterType<object, SettingsView>(nameof(SettingsView));
            Container.RegisterType<object, RunView>(nameof(RunView));
            Container.RegisterInstance(Container.Resolve<HasStarted>());
        }

        // Make sure to init the main window after modules are loaded
        protected override void InitializeModules()
        {
            base.InitializeModules();
            Shell = Container.Resolve<MainWindow>();
            Application.Current.MainWindow = Shell as Window;
            Application.Current.MainWindow.Show();
        }
    }
}

