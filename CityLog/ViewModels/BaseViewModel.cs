﻿using CityLog.Services.PhotoRepo;
using Microsoft.Practices.Unity;
using Prism.Mvvm;
using Prism.Regions;

namespace CityLog.ViewModels
{
    class BaseViewModel : BindableBase, INavigationAware
    {
        [Dependency]
        public IRegionManager RegionMgr { get; set; }

        [Dependency]
        public PhotoRepoFactory Factory { get; set; }

        public virtual bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public virtual void OnNavigatedFrom(NavigationContext navigationContext)
        { }

        public virtual void OnNavigatedTo(NavigationContext navigationContext)
        { }
    }
}
