﻿using CityLog.Views;
using Prism.Commands;

namespace CityLog.ViewModels
{
    class MainWindowViewModel : BaseViewModel
    {
        public DelegateCommand SettingsCmd { get; private set; }
        public DelegateCommand RunCmd { get; private set; }

        public MainWindowViewModel()
        {
            SettingsCmd = new DelegateCommand(OnSettingsCmd);
            RunCmd = new DelegateCommand(OnRunCmd);
        }

        private void OnSettingsCmd()
        {
            RegionMgr.RequestNavigate(nameof(MainWindow), nameof(SettingsView));
        }

        private void OnRunCmd()
        {
            RegionMgr.RequestNavigate(nameof(MainWindow), nameof(RunView));
        }
    }
}
