﻿using Prism.Regions;
using CityLog.Services.PhotoRepo;
using CityLog.Controls;
using CityLog.Collections;

namespace CityLog.ViewModels
{
    class SettingsViewModel : BaseViewModel
    {
        public PhotoRepoCollection RepoCollection { get; private set; }

        public SettingsViewModel()
        {
            RepoCollection = new PhotoRepoCollection();
        }

        public override void OnNavigatedTo(NavigationContext navigationContext)
        {
            base.OnNavigatedTo(navigationContext);
            foreach(IPhotoRepo repo in Factory.PhotoRepos)
            {
                var repoCtrl = new PhotoRepoCtrl(repo);
                RepoCollection.Add(repoCtrl);
            }
        }

        public override void OnNavigatedFrom(NavigationContext navigationContext)
        {
            base.OnNavigatedFrom(navigationContext);
            RepoCollection.Clear();
        }
    }
}
