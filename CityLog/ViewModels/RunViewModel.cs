﻿using CityLog.Services.GeoCoding;
using CityLog.Services.PhotoRepo;
using Prism.Commands;
using System;
using System.Linq;
using System.Collections.Generic;
using CityLog.DataTypes;
using Microsoft.Practices.Unity;
using CityLog.Services.KmlGen;

namespace CityLog.ViewModels
{
    class RunViewModel : BaseViewModel
    {
        [Dependency]
        public IKmlGen MyKmlGen { get; set; }

        public DelegateCommand StartCmd { get; private set; }
        public HasStarted Started { get; private set; }
        private string _photoRepoName;
        private string _repoPhotosProcessed;
        private string _totalPhotosProcessed;
        private string _photoCity;
        private IGeoCode _geocode;

        public RunViewModel(IGeoCode geocode, HasStarted started)
        {
            StartCmd = new DelegateCommand(OnStartCmd);
            Started = started;
            _geocode = geocode;
        }

        public string PhotoRepoName
        {
            get { return _photoRepoName; }
            set { SetProperty(ref _photoRepoName, value); }
        }

        public string RepoPhotosProcessed
        {
            get { return _repoPhotosProcessed; }
            set { SetProperty(ref _repoPhotosProcessed, value); }
        }

        public string PhotoCity
        {
            get { return _photoCity; }
            set { SetProperty(ref _photoCity, value); }
        }

        public string TotalPhotosProcessed
        {
            get { return _totalPhotosProcessed; }
            set { SetProperty(ref _totalPhotosProcessed, value); }
        }

        private async void OnStartCmd()
        {
            var photos = new List<KmlPhoto>();
            var cities = new Dictionary<int, List<string>>();
            int totalPhotoCount = 0;
            foreach (IPhotoRepo repo in Factory.PhotoRepos)
            {
                if(!repo.RepoEnabled || !repo.IsLoggedIn) continue;

                PhotoRepoName = repo.Title;

                int repoPhotoCount = 0;
                var action = new EventHandler<IRepoPhoto>((sender, photo) => 
                {
                    System.Threading.Interlocked.Increment(ref repoPhotoCount);
                    System.Threading.Interlocked.Increment(ref totalPhotoCount);
                    RepoPhotosProcessed = repoPhotoCount.ToString();
                    TotalPhotosProcessed = totalPhotoCount.ToString();
                    string city = _geocode.NearestPlaceName(photo.Latitude, photo.Longitude);
                    if (!string.IsNullOrWhiteSpace(city))
                    {
                        if (!cities.ContainsKey(photo.Taken.Year))
                            cities[photo.Taken.Year] = new List<string>();

                        if (!cities[photo.Taken.Year].Any(c => c == city))
                        {
                            cities[photo.Taken.Year].Add(city);
                            photos.Add(new KmlPhoto(photo, city));
                        }
                        PhotoCity = city;
                    }
                });

                repo.PhotoDidLoad += action;
                await repo.GetPhotos();
                repo.PhotoDidLoad -= action;

                await MyKmlGen.Load(photos);

                if (System.IO.File.Exists("placemarks.kml"))
                    System.IO.File.Delete("placemarks.kml");
                System.IO.File.Move(MyKmlGen.KmlPath.AbsolutePath, "placemarks.kml");
            }
        }
    }
}
