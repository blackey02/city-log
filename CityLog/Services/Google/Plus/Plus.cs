﻿using CityLog.Services.Google.Common;
using Google.Apis.Plus.v1;
using Google.Apis.Plus.v1.Data;
using System;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CityLog.Services.Google.Plus
{
    class Plus : ServiceBase, IPlus
    {
        private Person _user;

        public Plus()
        { }

        public ImageSource Avatar
        {
            get
            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(_user?.Image.Url, UriKind.Absolute);
                bitmap.EndInit();
                return bitmap;
            }
        }

        public async override Task Init(string accessToken)
        {
            await base.Init(accessToken);
            var service = new PlusService();
            var request = service.People.Get("me");
            request.OauthToken = _accessToken;
            _user = request.Execute();
        }

        public async override Task Reset()
        {
            await base.Reset();
            _user = null;
        }
    }
}
