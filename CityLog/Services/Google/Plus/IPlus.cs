﻿using CityLog.Services.Google.Common;
using System.Windows.Media;

namespace CityLog.Services.Google.Plus
{
    interface IPlus : IService
    {
        ImageSource Avatar { get; }
    }
}
