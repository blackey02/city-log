﻿using CityLog.Services.Google.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.Google.Photos
{
    interface IPhotos : IService
    {
        event EventHandler<Photo> PhotoDidLoad;
        Task<List<Album>> GetAlbums();
        Task GetPhotos();
    }
}
