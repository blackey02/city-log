﻿using CityLog.DataTypes;
using CityLog.Services.PhotoRepo;
using Google.GData.Photos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CityLog.Services.Google.Photos
{
    class Photo : IRepoPhoto
    {
        private PicasaEntry _photoEntry;
        private PhotoAccessor _photoAccessor;

        public Photo(PicasaEntry photoEntry)
        {
            _photoEntry = photoEntry;
            _photoAccessor = new PhotoAccessor(_photoEntry);
        }

        public string Id
        {
            get { return _photoAccessor.Id; }
        }

        #region IRepoPhoto
        public string Title
        {
            get { return _photoAccessor.PhotoTitle; }
        }

        public DateTime Taken
        {
            get { return Constants.Epoch.AddMilliseconds(_photoAccessor.Timestamp); }
        }

        public Size Size
        {
            get
            {
                return new Size(_photoAccessor.Width, _photoAccessor.Height);
            }
        }

        public double Latitude
        {
            get
            {
                return _photoAccessor.Latitude;
            }
        }

        public double Longitude
        {
            get
            {
                return _photoAccessor.Longitude;
            }
        }
        #endregion
    }
}
