﻿using Google.GData.Photos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CityLog.Services.Google.Photos
{
    class Album
    {
        public event EventHandler<Photo> PhotoLoaded;
        private PicasaEntry _albumEntry;
        private AlbumAccessor _albumAccessor;

        public Album(PicasaEntry albumEntry)
        {
            _albumEntry = albumEntry;
            _albumAccessor = new AlbumAccessor(_albumEntry);
        }

        private PicasaService Service
        {
            get { return _albumEntry.Service as PicasaService; }
        }

        private void FirePhotoLoaded(Photo photo)
        {
            PhotoLoaded?.Invoke(this, photo);
        }

        #region IAlbum
        public string Id
        {
            get { return _albumAccessor.Id; }
        }

        public string Title
        {
            get { return _albumAccessor.AlbumTitle; }
        }

        public async Task GetPhotos()
        {
            PicasaFeed photoFeed;
            int numTasks = (int)Math.Ceiling(_albumAccessor.NumPhotos / 1000.0);
            var tasks = new List<Task>(numTasks);
            for(int i = 0; i < numTasks; i++)
            {
                int ii = i;
                var task = Task.Run(() => {
                    try
                    {
                        PhotoQuery photoQuery = new PhotoQuery(PicasaQuery.CreatePicasaUri("default", Id));
                        photoQuery.StartIndex = ii * 1000;
                        photoQuery.NumberToRetrieve = 1000;
                        photoFeed = Service.Query(photoQuery);
                        foreach (PicasaEntry photoEntry in photoFeed.Entries) {
                            var photo = new Photo(photoEntry);
                            FirePhotoLoaded(photo);
                        }
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine($"GetPhotos threw while getting photos for {Title} - {e.Message}");
                    }
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }
        #endregion
    }
}
