﻿using CityLog.Services.Google.Common;
using Google.GData.Client;
using Google.GData.Photos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CityLog.Services.Google.Photos
{
    class Photos : ServiceBase, IPhotos
    {
        public event EventHandler<Photo> PhotoDidLoad;
        private PicasaService _service;

        public Photos()
        { }

        public async override Task Init(string accessToken)
        {
            await base.Init(accessToken);
            var authFactory = new GAuthSubRequestFactory(PicasaService.GPicasaService, "Blackey-CityLog-1.0.0");
            authFactory.Token = _accessToken;
            _service = new PicasaService(authFactory.ApplicationName);
            _service.RequestFactory = authFactory;
        }

        public async override Task Reset()
        {
            await base.Reset();
            _service = null;
        }

        private void FirePhotoDidLoad(Photo photo)
        {
            PhotoDidLoad?.Invoke(this, photo);
        }

        #region IPhotos
        public async Task<List<Album>> GetAlbums()
        {
            var albums = new List<Album>();
            if(_service == null) return albums;

            AlbumQuery albumsQuery = new AlbumQuery(PicasaQuery.CreatePicasaUri("default"));
            albumsQuery.NumberToRetrieve = 0;
            albumsQuery.ExtraParameters = "showall";
            int numAlbums = 0;
            await Task.Run(() => numAlbums = _service.Query(albumsQuery).TotalResults).ConfigureAwait(false);

            int numTasks = (int)Math.Ceiling(numAlbums / 1000.0);
            var tasks = new List<Task>(numTasks);
            for (int i = 0; i < numTasks; i++)
            {
                int ii = i;
                var task = Task.Run(() => {
                    try
                    {
                        AlbumQuery albumQuery = new AlbumQuery(PicasaQuery.CreatePicasaUri("default"));
                        albumQuery.StartIndex = ii * 1000;
                        albumQuery.NumberToRetrieve = 1000;
                        albumQuery.ExtraParameters = "showall";
                        PicasaFeed albumFeed = _service.Query(albumQuery);
                        foreach (PicasaEntry albumEntry in albumFeed.Entries)
                        {
                            var album = new Album(albumEntry);
                            albums.Add(album);
                        }
                    }
                    catch
                    {
                        Console.WriteLine($"GetAlbums threw while getting albums");
                    }
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks).ConfigureAwait(false);
            return albums;
        }

        public async Task GetPhotos()
        {
            if (_service == null) return;

            var albums = await GetAlbums().ConfigureAwait(false);;
            int numTasks = albums.Count;
            var tasks = new List<Task>(numTasks);
            for(int i = 0; i < numTasks; i++)
            {
                int ii = i;
                var task = Task.Run(async () => {
                    var album = albums[ii];
                    var handler = new EventHandler<Photo>((sender, photo) => FirePhotoDidLoad(photo));
                    album.PhotoLoaded += handler;
                    try {
                        await album.GetPhotos().ConfigureAwait(false);
                    }
                    catch (Exception e) {
                        Console.WriteLine($"Failed to get photos from album {album.Title} - {e.Message}");
                    }
                    album.PhotoLoaded -= handler;
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks).ConfigureAwait(false);
        }
        #endregion
    }
}
