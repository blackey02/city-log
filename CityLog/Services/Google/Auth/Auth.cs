﻿using CityLog.Services.Google.Common;
using CityLog.Services.PhotoRepo;
using Google.GData.Client;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CityLog.Services.Google.Auth
{
    class Auth : IAuth
    {
        private ServiceFactory _svcFactory;
        private OAuth2Parameters _oAuthParams;

        public Auth(ServiceFactory svcFactory)
        {
            _svcFactory = svcFactory;
        }

        public bool IsLoggedIn
        {
            get { return !string.IsNullOrWhiteSpace(_oAuthParams?.AccessToken); }
        }

        private bool InitRequired
        {
            get { return string.IsNullOrEmpty(_oAuthParams?.ClientId); }
        }

        public async Task<LoginResult> Login()
        {
            LoginResult result = LoginResult.Success;
            try
            {
                if(InitRequired) await InitOAuth();
                OAuthUtil.RefreshAccessToken(_oAuthParams);
                await _svcFactory.Init(_oAuthParams.AccessToken).ConfigureAwait(false);
                Properties.Settings.Default.GoogleRefreshToken = _oAuthParams.RefreshToken;
                Properties.Settings.Default.Save();
            }
            catch
            {
                var oauthUrl = OAuthUtil.CreateOAuth2AuthorizationUrl(_oAuthParams);
                Process.Start(oauthUrl);
                result = LoginResult.AuthNeeded;
            }
            return result;
        }

        public async Task<LoginResult> Login(string accessCode)
        {
            LoginResult result = LoginResult.Success;
            try
            {
                if (InitRequired) await InitOAuth();
                _oAuthParams.RefreshToken = null;
                _oAuthParams.AccessCode = accessCode;
                await Task.Run(() => OAuthUtil.GetAccessToken(_oAuthParams)).ConfigureAwait(false);
                await Login().ConfigureAwait(false);
            }
            catch
            {
                result = LoginResult.Fail;
            }
            return result;
        }

        public Task<string> GetAccessToken()
        {
            return Task.FromResult(_oAuthParams?.AccessToken);
        }

        public async Task Logout()
        {
            _oAuthParams = null;
            await _svcFactory.Reset().ConfigureAwait(false);
            Properties.Settings.Default.GoogleRefreshToken = null;
            Properties.Settings.Default.GoogleLastQuery = default(DateTime);
            Properties.Settings.Default.Save();
        }

        private async Task InitOAuth()
        {
            _oAuthParams = await Task.Run(() => new OAuth2Parameters()).ConfigureAwait(false);
            _oAuthParams.ClientId = "_Client id from google developer console_";
            _oAuthParams.ClientSecret = "_Client secret from google developer console_";
            _oAuthParams.Scope = "http://picasaweb.google.com/data https://www.googleapis.com/auth/plus.me";
            _oAuthParams.AuthUri = "https://accounts.google.com/o/oauth2/auth";
            _oAuthParams.TokenUri = "https://accounts.google.com/o/oauth2/token";
            _oAuthParams.RedirectUri = "_Redirect uri from google developer console_";
            _oAuthParams.RefreshToken = Properties.Settings.Default.GoogleRefreshToken;
        }
    }
}
