﻿using CityLog.Services.PhotoRepo;
using System.Threading.Tasks;

namespace CityLog.Services.Google.Auth
{
    interface IAuth
    {
        bool IsLoggedIn { get; }
        Task<LoginResult> Login();
        Task<LoginResult> Login(string accessCode);
        Task<string> GetAccessToken();
        Task Logout();
    }
}
