﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CityLog.Services.PhotoRepo;
using CityLog.Services.Google.Auth;
using CityLog.Services.Google.Plus;
using CityLog.Services.Google.Photos;
using System.Windows.Media;
using System;
using System.Windows.Media.Imaging;

namespace CityLog.Services.Google
{
    class GoogleRepo : IGoogleRepo
    {
        public event EventHandler<IRepoPhoto> PhotoDidLoad;
        private IAuth _auth;
        private IPlus _plus;
        private IPhotos _photos;

        public GoogleRepo(IAuth auth, IPlus plus, IPhotos photos)
        {
            _auth = auth;
            _plus = plus;
            _photos = photos;
            _photos.PhotoDidLoad += Photos_PhotoDidLoad;
        }

        private void Photos_PhotoDidLoad(object sender, Photo e)
        {
            PhotoDidLoad?.Invoke(this, e);
        }

        #region IGoogleRepo
        public RepoType PhotoRepo
        {
            get { return RepoType.Google; }
        }

        public string Title
        {
            get { return "Google"; }
        }

        public ImageSource RepoAvatar
        {
            get
            {
                var avatar = new BitmapImage();
                avatar.BeginInit();
                avatar.UriSource = new Uri("pack://application:,,,/CityLog;component/Resources/GoogleRepo.png");
                avatar.EndInit();
                return avatar;
            }
        }

        public ImageSource UserAvatar
        {
            get { return _plus.Avatar; }
        }

        public bool RepoEnabled
        {
            get { return Properties.Settings.Default.GoogleEnabled; }
            set { Properties.Settings.Default.GoogleEnabled = value; Properties.Settings.Default.Save(); }
        }

        public bool IsLoggedIn
        {
            get { return _auth.IsLoggedIn; }
        }

        public LoginAuthType AuthType
        {
            get { return LoginAuthType.OAuth; }
        }

        public async Task<LoginResult> OAuthLogin()
        {
            var result = await _auth.Login().ConfigureAwait(false);
            return result;
        }

        public async Task<LoginResult> OAuthLogin(string accessCode)
        {
            var result = await _auth.Login(accessCode).ConfigureAwait(false);
            return result;
        }

        public async Task GetPhotos()
        {
            await _photos.GetPhotos().ConfigureAwait(false);
        }

        public async Task Logout()
        {
            await _auth.Logout().ConfigureAwait(false);
        }
        #endregion
    }
}
