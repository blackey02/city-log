﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.Google.Common
{
    abstract class ServiceBase : IService
    {
        protected string _accessToken;

        protected ServiceBase()
        { }

        public virtual Task Init(string accessToken)
        {
            _accessToken = accessToken;
            return Task.FromResult(true);
        }

        public virtual Task Reset()
        {
            _accessToken = string.Empty;
            return Task.FromResult(true);
        }
    }
}
