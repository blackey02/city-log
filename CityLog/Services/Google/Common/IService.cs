﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.Google.Common
{
    interface IService
    {
        Task Init(string accessToken);
        Task Reset();
    }
}
