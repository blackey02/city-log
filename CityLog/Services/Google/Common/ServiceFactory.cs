﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.Google.Common
{
    class ServiceFactory : ServiceBase
    {
        private Func<string, IService> _factory;

        public ServiceFactory(Func<string, IService> factory)
        {
            _factory = factory;
        }

        private List<IService> AllServices
        {
            get
            {
                var services = new List<IService>();
                services.Add(_factory(nameof(Plus)));
                services.Add(_factory(nameof(Photos)));
                return services;
            }
        }

        public override Task Init(string accessToken)
        {
            base.Init(accessToken);
            AllServices.ForEach(service => service.Init(accessToken));
            return Task.FromResult(true);
        }
    }
}
