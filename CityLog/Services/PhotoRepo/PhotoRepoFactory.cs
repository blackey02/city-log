﻿using CityLog.Services.Google;
using System.Collections.Generic;

namespace CityLog.Services.PhotoRepo
{
    class PhotoRepoFactory
    {
        private IGoogleRepo _googleRepo;

        public PhotoRepoFactory(IGoogleRepo googleRepo)
        {
            _googleRepo = googleRepo;
        }

        public List<IPhotoRepo> PhotoRepos
        {
            get
            {
                var repos = new List<IPhotoRepo>();
                repos.Add(_googleRepo);
                return repos;
            }
        }
    }
}
