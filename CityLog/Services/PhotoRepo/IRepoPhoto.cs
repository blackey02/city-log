﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CityLog.Services.PhotoRepo
{
    interface IRepoPhoto
    {
        string Title { get; }
        DateTime Taken { get; }
        Size Size { get; }
        double Latitude { get; }
        double Longitude { get; }
    }
}
