﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CityLog.Services.PhotoRepo
{
    enum RepoType
    {
        Google
    }

    enum LoginAuthType
    {
        OAuth
    }

    enum LoginResult
    {
        Fail,
        Success,
        AuthNeeded,
        NotSupported
    }

    interface IPhotoRepo
    {
        event EventHandler<IRepoPhoto> PhotoDidLoad;
        RepoType PhotoRepo { get; }
        string Title { get; }
        ImageSource RepoAvatar { get; }
        ImageSource UserAvatar { get; }
        bool RepoEnabled { get; set; }
        bool IsLoggedIn { get; }
        LoginAuthType AuthType { get; }
        Task<LoginResult> OAuthLogin();
        Task<LoginResult> OAuthLogin(string accessCode);
        Task GetPhotos();
        Task Logout();
    }
}
