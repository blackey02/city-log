﻿using CityLog.Services.PhotoRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.KmlGen
{
    class KmlPhoto
    {
        public string City { get; set; }
        public IRepoPhoto RepoPhoto { get; private set; }

        public KmlPhoto(IRepoPhoto repoPhoto, string city = "")
        {
            RepoPhoto = repoPhoto;
            City = city;
        }
    }
}
