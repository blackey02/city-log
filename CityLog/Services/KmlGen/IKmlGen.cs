﻿using CityLog.Services.PhotoRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.KmlGen
{
    interface IKmlGen
    {
        Uri KmlPath { get; set; }
        Task Load(List<KmlPhoto> photos);
    }
}
