﻿using CityLog.Services.PhotoRepo;
using MoreLinq;
using SharpKml.Base;
using SharpKml.Dom;
using SharpKml.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.KmlGen
{
    class KmlGen : IKmlGen
    {
        public Uri KmlPath { get; set; }
        private Document _doc;

        public KmlGen()
        {
            KmlPath = new Uri(Path.GetTempFileName());
            _doc = new Document();
            _doc.Name = "City Log";
        }

        public Task Load(List<KmlPhoto> photos)
        {
            var color = Color32.Parse("ffcfa4c6");
            var years = photos.GroupBy(p => p.RepoPhoto.Taken.Year);

            AddStyle(color);
            foreach (var year in years)
            {
                var folder = new Folder();
                folder.Name = year.First().RepoPhoto.Taken.Year.ToString();
                foreach(var photo in year)
                {
                    var point = new Point();
                    point.Coordinate = new Vector(photo.RepoPhoto.Latitude, photo.RepoPhoto.Longitude);

                    var placemark = new Placemark();
                    placemark.Geometry = point;
                    placemark.Name = photo.City;
                    placemark.Description = new Description();
                    placemark.Description.Text = photo.RepoPhoto.Taken.ToString("D");
                    placemark.StyleUrl = new Uri($"#{color.ToString()}", UriKind.Relative);

                    folder.AddFeature(placemark);
                }
                _doc.AddFeature(folder);
            }

            using (var stream = File.OpenWrite(KmlPath.AbsolutePath))
            {
                var kml = new Kml();
                kml.Feature = _doc;
                var kmlFile = KmlFile.Create(kml, false);
                kmlFile.Save(stream);
            }
            return Task.FromResult(true);
        }

        private void AddStyle(Color32 color)
        {
            var styleFound = _doc.Styles.FirstOrDefault(s => s.Id == color.ToString());
            if(styleFound != null) return;

            var stylemap = new StyleMapCollection();
            stylemap.Id = color.ToString();

            var style = new Style();
            style.Id = $"{color.ToString()}-normal";
            style.Icon = new IconStyle();
            style.Icon.Color = color;
            style.Icon.Scale = 1.0;
            style.Icon.Icon = new IconStyle.IconLink(new Uri("http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png"));
            style.Label = new LabelStyle();
            style.Label.Color = Color32.Parse("00ffffff"); ;
            _doc.AddStyle(style);

            var normalPair = new Pair();
            normalPair.StyleUrl = new Uri($"#{color.ToString()}-normal", UriKind.Relative);
            normalPair.State = StyleState.Normal;
            stylemap.Add(normalPair);

            style = new Style();
            style.Id = $"{color.ToString()}-highlight";
            style.Icon = new IconStyle();
            style.Icon.Color = color;
            style.Icon.Scale = 1.0;
            style.Icon.Icon = new IconStyle.IconLink(new Uri("http://www.gstatic.com/mapspro/images/stock/503-wht-blank_maps.png"));
            style.Label = new LabelStyle();
            style.Label.Color = Color32.Parse("00ffffff");
            _doc.AddStyle(style);

            var highlightPair = new Pair();
            highlightPair.StyleUrl = new Uri($"#{color.ToString()}-highlight", UriKind.Relative);
            highlightPair.State = StyleState.Highlight;
            stylemap.Add(highlightPair);
            _doc.AddStyle(stylemap);
        }
    }
}
