﻿using CityLog.DataTypes;
using GeoSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.GeoCoding
{
    class GeoCode : IGeoCode
    {
        private ReverseGeoCode _geocode;
        private HasStarted _started;

        public GeoCode(HasStarted started)
        {
            _started = started;
            System.Threading.ThreadPool.QueueUserWorkItem(_ => Load());
        }

        private void Load()
        {
            var stream = new MemoryStream(Properties.Resources.cities1000);
            _geocode = new ReverseGeoCode(stream, false);
            _started.GeoCodes = true;
        }

        public string NearestPlaceName(double latitude, double longitude)
        {
            string city = string.Empty;
            var place = _geocode.NearestPlace(latitude, longitude);
            if(place != null)
            {
                if(place.FeatureClass == GeoFeatureClass.City)
                {
                    city = place.Name;
                }
            }
            city = city == "Takoradi" ? string.Empty : city;
            return city;
        }
    }
}
