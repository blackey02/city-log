﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityLog.Services.GeoCoding
{
    interface IGeoCode
    {
        string NearestPlaceName(double latitude, double longitude);
    }
}
