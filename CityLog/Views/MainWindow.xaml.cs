﻿using CityLog.ViewModels;
using Microsoft.Practices.Unity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CityLog.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    partial class MainWindow : Window
    {
        private IRegionManager _regionMgr;

        public MainWindow(MainWindowViewModel viewModel, IRegionManager regionMgr)
        {
            InitializeComponent();
            DataContext = viewModel;
            _regionMgr = regionMgr;
            Loaded += MainWindow_Loaded;
            RegionManager.SetRegionManager(regionManager, regionMgr);
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _regionMgr.RequestNavigate(nameof(MainWindow), nameof(SettingsView));
        }
    }
}
