﻿using CityLog.Services.PhotoRepo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CityLog.Controls
{
    /// <summary>
    /// Interaction logic for PhotoRepoCtrl.xaml
    /// </summary>
    partial class PhotoRepoCtrl : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public IPhotoRepo PhotoRepo { get; private set; }
        private bool _didLoad;

        public PhotoRepoCtrl(IPhotoRepo photoRepo)
        {
            InitializeComponent();
            PhotoRepo = photoRepo;
        }

        public bool DidLoad
        {
            get { return _didLoad; }
            set { _didLoad = value; FirePropertyChanged(nameof(DidLoad)); }
        }

        public bool RepoEnabled
        {
            get { return PhotoRepo.RepoEnabled; }
        }

        public bool ShowOAuthDetails
        {
            get
            {
                bool show = true;
                show = show && PhotoRepo.RepoEnabled;
                show = show && PhotoRepo.AuthType == LoginAuthType.OAuth;
                show = show && !PhotoRepo.IsLoggedIn;
                return show;
            }
        }

        private void FirePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private async void OAuthAuthCode_Click(object sender, RoutedEventArgs e)
        {
            LoginResult result = await PhotoRepo.OAuthLogin(authCode.Text);

            FirePropertyChanged(nameof(ShowOAuthDetails));
            FirePropertyChanged(nameof(RepoEnabled));
        }

        private async void ToggleRepo_Click(object sender, RoutedEventArgs e)
        {
            PhotoRepo.RepoEnabled = !PhotoRepo.RepoEnabled;
            if(PhotoRepo.RepoEnabled)
            {
                if (PhotoRepo.AuthType == LoginAuthType.OAuth)
                    await PhotoRepo.OAuthLogin();
            }
            else
                await PhotoRepo.Logout();

            FirePropertyChanged(nameof(ShowOAuthDetails));
            FirePropertyChanged(nameof(RepoEnabled));
        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if(RepoEnabled)
            {
                if(PhotoRepo.AuthType == LoginAuthType.OAuth)
                    await PhotoRepo.OAuthLogin();
            }

            FirePropertyChanged(nameof(ShowOAuthDetails));
            FirePropertyChanged(nameof(RepoEnabled));
            DidLoad = true;
        }
    }
}
